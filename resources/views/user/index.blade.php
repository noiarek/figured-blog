@extends('layouts.base')

@section('title', 'User')

@section('subtitle', 'All')

@section('content')
    <div class="text-center">
        <a class="btn btn-primary" href="{{ route('users.create') }}">Create new user</a>
    </div>

    @if(count($users))
    <!-- start project list -->
    <table class="table table-striped projects">
        <thead>
            <tr>
                <th style="width: 20%">Name</th>
                <th>Email</th>
                <th>Role</th>
                <th style="width: 20%">#Edit</th>
            </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
        <tr>
            <td>
                {{ $user->name }}
            </td>
            <td>
                {{ $user->email }}
            </td>
            <td>
                {{ $user->role }}
            </td>
            <td>
                <form method="POST" action="{{ route('users.destroy', ['user' => $user->id]) }}">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <a href="{{ route('users.show', ['user' => $user->id]) }}" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit </a>
                    @if($user->id !== Auth::user()->id)
                        <button type="submit" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </button>
                    @endif
                </form>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
    <!-- end project list -->
    @else
        <div class="text-center" style="margin-top: 30px">
            <strong>There isn't users yet!</strong>
        </div>
    @endif
@endsection