<?php

use Illuminate\Database\Seeder;

class CategoriesAndPostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection('mongodb')
            ->collection('categories')
            ->insert(['name' => 'No category']);

        DB::connection('mongodb')
            ->collection('categories')
            ->insert(['name' => 'One more category']);
    }
}
