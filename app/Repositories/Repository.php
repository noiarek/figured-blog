<?php
/**
 * Created by PhpStorm.
 * User: rv
 * Date: 23/07/17
 * Time: 7:41 PM
 */

namespace App\Repositories;


use App\Models\Post;

abstract class Repository
{
    /**
     * The class model implementation.
     *
     * @var Model
     */
    protected $entity;

    abstract public function getClass();

    public function create($data, $save = true)
    {
        $this->entity->fill($data);

        if($save) {
            $this->entity->save();
        }

        return $this->entity;
    }

    public function all()
    {
        return $this->getClass()::all();
    }

    public function update($entity, $data)
    {
        $this->entity = $entity;
        return $this->create($data);
    }

    public function delete($entity)
    {
        $entity->delete();
        return null;
    }
}