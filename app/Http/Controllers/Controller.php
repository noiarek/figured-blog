<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    const SINGULAR = null;
    const PLURAL   = null;

    protected $repository;

    public function redirectHome()
    {
        return redirect()->route(static::PLURAL . '.index');
    }

    public function index()
    {
        return view(static::SINGULAR . '.index', [
            static::PLURAL => $this->repository->all()
        ]);
    }

    public function create()
    {
        return view(static::SINGULAR . '.create');
    }

    public function afterShow(Model $entity)
    {
        return view(static::SINGULAR . '.show', [
            static::SINGULAR => $entity
        ]);
    }

    public function afterStore(Request $request)
    {
        $this->repository->create($request->all());
        return $this->redirectHome();
    }

    public function afterUpdate(Request $request, Model $entity)
    {
        $this->repository->update($entity, $request->all());
        return $this->redirectHome();
    }

    public function afterDestroy(Model $entity)
    {
        $this->repository->delete($entity);
        return $this->redirectHome();
    }
}
