<?php
/**
 * Created by PhpStorm.
 * User: rv
 * Date: 23/07/17
 * Time: 7:39 PM
 */


namespace App\Repositories;

use App\Models\Post;
use Illuminate\Support\Facades\Auth;

class PostRepository extends Repository
{
    /**
     * Create a new post repository instance.
     *
     * @param  Post post
     * @return void
     */
    public function __construct(Post $post)
    {
        $this->entity = $post;
    }

    public function getClass()
    {
        return Post::class;
    }

    public function create($data, $save = true)
    {
        $entity = parent::create($data, false);

        $user = Auth::user();
        $user->posts()->save($entity);

        return $this->entity;
    }

    public function getPublishedPost()
    {
        return $this->getClass()::where('is_published', "1")->get();
    }
}