@extends('layouts.base')

@section('title', 'Post')

@section('subtitle', 'All')

@section('content')
    <div class="text-center">
        <a class="btn btn-primary" href="{{ route('posts.create') }}">Create new article</a>
    </div>

    @if(count($posts))
    <!-- start project list -->
    <table class="table table-striped projects">
        <thead>
            <tr>
                <th style="width: 20%">Title</th>
                <th>Slug</th>
                <th>Body</th>
                <th>Author</th>
                <th>Category</th>
                <th>Status</th>
                <th style="width: 20%">#Edit</th>
            </tr>
        </thead>
        <tbody>
        @foreach($posts as $post)
        <tr>
            <td>
                {{ $post->title }}
            </td>
            <td>
                {{ $post->slug }}
            </td>
            <td>
                {{ str_limit($post->body, $limit = 30, $end = '...') }}
            </td>
            <td>
                {{ $post->user->name }}
            </td>
            <td>
                {{ $post->category->name }}
            </td>
            <td>
                <button type="button" class="btn btn-{{ $post->is_published ? 'success' : 'warning'}} btn-xs">{{ $post->is_published ? 'Published' : 'Unpublished'}}</button>
            </td>
            <td>
                <form method="POST" action="{{ route('posts.destroy', ['post' => $post->id]) }}">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <a href="{{ route('posts.show', ['post' => $post->id]) }}" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit </a>
                    <button type="submit" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </button>
                </form>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
    <!-- end project list -->
    @else
        <div class="text-center" style="margin-top: 30px">
            <strong>There isn't post yet!</strong>
        </div>
    @endif
@endsection