<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Http\Requests\Post as PostValidator;
use App\Repositories\CategoryRepository;
use App\Repositories\PostRepository;

class HomeController extends Controller
{
    private $categoriesRepository;

    /**
     * Create a new home controller instance.
     *
     * @param  PostRepository  posts
     * @return void
     */
    public function __construct(PostRepository $posts)
    {
        $this->repository = $posts;
    }

    public function index()
    {
        return view('home.index', [
            'posts' => $this->repository->getPublishedPost()
        ]);
    }

}