<?php
/**
 * Created by PhpStorm.
 * User: rv
 * Date: 23/07/17
 * Time: 6:39 PM
 */

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class Post extends Eloquent {

     use SoftDeletes;

     protected $connection = 'mongodb';

     protected $fillable = ['title', 'slug', 'is_published', 'body', 'user_id', 'category_id'];
     protected $dates = ['deleted_at'];

     public function user()
     {
         return $this->belongsTo(User::class);
     }

     public function category()
     {
        return $this->belongsTo(Category::class);
     }
}