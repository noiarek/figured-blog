<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Jenssegers\Mongodb\Eloquent\HybridRelations;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;
    use HybridRelations;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The connection for this model.
     *
     * @var string
     */
    protected $connection = 'mysql';

    /**
     * Relation between User and Posts
     *
     * @return Collection
     */
    public function posts()
    {
        return $this->hasMany(Post::class);
    }


    /**
     * Check if the user has certain role
     *
     * @param  string  $role
     * @return bool
     */
    public function hasRole($role) {
        return ($this->role === $role);
    }
}
