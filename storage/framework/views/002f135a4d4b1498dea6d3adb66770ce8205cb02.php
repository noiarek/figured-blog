<?php $__env->startSection('title', 'Category'); ?>

<?php $__env->startSection('subtitle', 'All'); ?>

<?php $__env->startSection('content'); ?>
    <div class="text-center">
        <a class="btn btn-primary" href="<?php echo e(route('categories.create')); ?>">Create new category</a>
    </div>

    <?php if(count($categories)): ?>
    <!-- start project list -->
    <table class="table table-striped projects">
        <thead>
            <tr>
                <th style="width: 20%">Name</th>
                <th style="width: 20%">#Edit</th>
            </tr>
        </thead>
        <tbody>
        <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td>
                <?php echo e($category->name); ?>

            </td>
            <td>
                <form method="POST" action="<?php echo e(route('categories.destroy', ['category' => $category->id])); ?>">
                    <?php echo e(csrf_field()); ?>

                    <?php echo e(method_field('DELETE')); ?>

                    <a href="<?php echo e(route('categories.show', ['category' => $category->id])); ?>" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit </a>
                    <button type="submit" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </button>
                </form>
            </td>
        </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </tbody>
    </table>
    <!-- end project list -->
    <?php else: ?>
        <div class="text-center" style="margin-top: 30px">
            <strong>There isn't categories yet!</strong>
        </div>
    <?php endif; ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.base', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>