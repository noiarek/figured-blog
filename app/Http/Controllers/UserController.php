<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Http\Requests\User as UserValidator;
use App\Repositories\UserRepository;

class UserController extends Controller
{
    const SINGULAR = 'user';
    const PLURAL = 'users';

    /**
     * Create a user controller instance.
     *
     * @param  UserRepository  users
     * @return void
     */
    public function __construct(UserRepository $users)
    {
        $this->repository = $users;
    }

    public function show(User $user)
    {
        return parent::afterShow($user);
    }

    public function update(UserValidator $request, User $user)
    {
        return parent::afterUpdate($request, $user);
    }

    public function store(UserValidator $request)
    {
        return parent::afterStore($request);
    }

    public function destroy(User $user)
    {
        return parent::afterDestroy($user);
    }
}