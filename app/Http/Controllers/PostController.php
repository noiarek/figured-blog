<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Http\Requests\Post as PostValidator;
use App\Repositories\CategoryRepository;
use App\Repositories\PostRepository;

class PostController extends Controller
{
    const SINGULAR = 'post';
    const PLURAL = 'posts';

    private $categoriesRepository;

    /**
     * Create a new post controller instance.
     *
     * @param  PostRepository  posts
     * @return void
     */
    public function __construct(PostRepository $posts, CategoryRepository $categories)
    {
        $this->repository = $posts;
        $this->categoriesRepository = $categories;
    }

    public function create()
    {
        return parent::create()
            ->with([
                'categories' => $this->categoriesRepository->all()
            ]);
    }

    public function show(Post $post)
    {
        return parent::afterShow($post);
    }

    public function update(PostValidator $request, Post $post)
    {
        return parent::afterUpdate($request, $post);
    }

    public function store(PostValidator $request)
    {
        return parent::afterStore($request);
    }

    public function destroy(Post $post)
    {
        return parent::afterDestroy($post);
    }
}