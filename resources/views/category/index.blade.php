@extends('layouts.base')

@section('title', 'Category')

@section('subtitle', 'All')

@section('content')
    <div class="text-center">
        <a class="btn btn-primary" href="{{ route('categories.create') }}">Create new category</a>
    </div>

    @if(count($categories))
    <!-- start project list -->
    <table class="table table-striped projects">
        <thead>
            <tr>
                <th style="width: 20%">Name</th>
                <th style="width: 20%">#Edit</th>
            </tr>
        </thead>
        <tbody>
        @foreach($categories as $category)
        <tr>
            <td>
                {{ $category->name }}
            </td>
            <td>
                <form method="POST" action="{{ route('categories.destroy', ['category' => $category->id]) }}">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <a href="{{ route('categories.show', ['category' => $category->id]) }}" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit </a>
                    <button type="submit" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </button>
                </form>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
    <!-- end project list -->
    @else
        <div class="text-center" style="margin-top: 30px">
            <strong>There isn't categories yet!</strong>
        </div>
    @endif
@endsection