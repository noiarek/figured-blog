<?php
/**
 * Created by PhpStorm.
 * User: rv
 * Date: 23/07/17
 * Time: 6:39 PM
 */

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class Category extends Eloquent {

     use SoftDeletes;

     protected $connection = 'mongodb';

     protected $fillable = ['name'];
     protected $dates = ['deleted_at'];

    public function posts()
    {
        return $this->embedsMany(Post::class);
    }
}