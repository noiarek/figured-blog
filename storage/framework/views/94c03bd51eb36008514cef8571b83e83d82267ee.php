<?php $__env->startSection('title', 'Post'); ?>

<?php $__env->startSection('subtitle', 'All'); ?>

<?php $__env->startSection('content'); ?>
    <div class="text-center">
        <a class="btn btn-primary" href="<?php echo e(route('posts.create')); ?>">Create new article</a>
    </div>

    <?php if(count($posts)): ?>
    <!-- start project list -->
    <table class="table table-striped projects">
        <thead>
            <tr>
                <th style="width: 20%">Title</th>
                <th>Slug</th>
                <th>Body</th>
                <th>Author</th>
                <th>Category</th>
                <th>Status</th>
                <th style="width: 20%">#Edit</th>
            </tr>
        </thead>
        <tbody>
        <?php $__currentLoopData = $posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td>
                <?php echo e($post->title); ?>

            </td>
            <td>
                <?php echo e($post->slug); ?>

            </td>
            <td>
                <?php echo e(str_limit($post->body, $limit = 30, $end = '...')); ?>

            </td>
            <td>
                <?php echo e($post->user->name); ?>

            </td>
            <td>
                <?php echo e($post->category->name); ?>

            </td>
            <td>
                <button type="button" class="btn btn-<?php echo e($post->is_published ? 'success' : 'warning'); ?> btn-xs"><?php echo e($post->is_published ? 'Published' : 'Unpublished'); ?></button>
            </td>
            <td>
                <form method="POST" action="<?php echo e(route('posts.destroy', ['post' => $post->id])); ?>">
                    <?php echo e(csrf_field()); ?>

                    <?php echo e(method_field('DELETE')); ?>

                    <a href="<?php echo e(route('posts.show', ['post' => $post->id])); ?>" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit </a>
                    <button type="submit" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </button>
                </form>
            </td>
        </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </tbody>
    </table>
    <!-- end project list -->
    <?php else: ?>
        <div class="text-center" style="margin-top: 30px">
            <strong>There isn't post yet!</strong>
        </div>
    <?php endif; ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.base', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>