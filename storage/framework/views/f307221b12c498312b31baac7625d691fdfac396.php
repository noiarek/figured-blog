<?php $__env->startSection('title', 'User'); ?>

<?php $__env->startSection('subtitle', 'All'); ?>

<?php $__env->startSection('content'); ?>
    <div class="text-center">
        <a class="btn btn-primary" href="<?php echo e(route('users.create')); ?>">Create new user</a>
    </div>

    <?php if(count($users)): ?>
    <!-- start project list -->
    <table class="table table-striped projects">
        <thead>
            <tr>
                <th style="width: 20%">Name</th>
                <th>Email</th>
                <th>Role</th>
                <th style="width: 20%">#Edit</th>
            </tr>
        </thead>
        <tbody>
        <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td>
                <?php echo e($user->name); ?>

            </td>
            <td>
                <?php echo e($user->email); ?>

            </td>
            <td>
                <?php echo e($user->role); ?>

            </td>
            <td>
                <form method="POST" action="<?php echo e(route('users.destroy', ['user' => $user->id])); ?>">
                    <?php echo e(csrf_field()); ?>

                    <?php echo e(method_field('DELETE')); ?>

                    <a href="<?php echo e(route('users.show', ['user' => $user->id])); ?>" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit </a>
                    <?php if($user->id !== Auth::user()->id): ?>
                        <button type="submit" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </button>
                    <?php endif; ?>
                </form>
            </td>
        </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </tbody>
    </table>
    <!-- end project list -->
    <?php else: ?>
        <div class="text-center" style="margin-top: 30px">
            <strong>There isn't users yet!</strong>
        </div>
    <?php endif; ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.base', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>