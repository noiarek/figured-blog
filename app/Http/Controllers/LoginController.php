<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\Login;
use Illuminate\Support\Facades\Auth;


class LoginController extends Controller
{
    public function index()
    {
        return view('layouts.login');
    }

    public function store(Login $request)
    {
        if (Auth::attempt($request->only(['email', 'password']))) {
            if(Auth::user()->hasRole('admin')) {
                return redirect()->route('posts.index');
            } else {
                return redirect()->route('home');
            }
        } else {
            return back()
                ->withErrors(['Email/Password are incorrect']);
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('home');
    }
}