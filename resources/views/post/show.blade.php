@extends('layouts.base')

@section('title', 'Post')

@section('subtitle', 'create')

@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_content">
                    <br />
                    <form method="POST" action="{{ route('posts.update', ['post' => $post->id]) }}" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">
                        @include('layouts.message')
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="title">Title <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="title" name="title" required="required" class="form-control col-md-7 col-xs-12" value="{{ $post->title }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="slug">Slug
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="slug" name="slug" class="form-control col-md-7 col-xs-12" value="{{ $post->slug }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Published</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div id="gender" class="btn-group" data-toggle="buttons">
                                        <input type="radio" name="is_published" value="1" {{ $post->is_published ? 'checked="checked"' : '' }}> &nbsp; Yes &nbsp;
                                        <input type="radio" name="is_published" value="0" {{ !$post->is_published ? 'checked="checked"' : '' }}> &nbsp; No &nbsp;
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Body <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <textarea id="body" name="body" class="date-picker form-control col-md-7 col-xs-12" required="required">{{ $post->body }}</textarea>
                            </div>
                        </div>
                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <a href="{{ route('posts.index') }}" class="btn btn-primary" type="button">Cancel</a>
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection