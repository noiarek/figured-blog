<?php
/**
 * Created by PhpStorm.
 * User: rv
 * Date: 23/07/17
 * Time: 7:39 PM
 */


namespace App\Repositories;

use App\Models\Category;

class CategoryRepository extends Repository
{
    /**
     * Create a new category repository instance.
     *
     * @param  Category $category
     * @return void
     */
    public function __construct(Category $category)
    {
        $this->entity = $category;
    }

    public function getClass()
    {
        return Category::class;
    }
}