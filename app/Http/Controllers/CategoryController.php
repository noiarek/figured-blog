<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Http\Requests\Category as CategoryValidator;
use App\Repositories\CategoryRepository;

class CategoryController extends Controller
{
    const SINGULAR = 'category';
    const PLURAL = 'categories';

    /**
     * Create a new category controller instance.
     *
     * @param  CategoryRepository  categories
     * @return void
     */
    public function __construct(CategoryRepository $categories)
    {
        $this->repository = $categories;
    }

    public function show(Category $category)
    {
        return parent::afterShow($category);
    }

    public function update(CategoryValidator $request, Category $category)
    {
        return parent::afterUpdate($request, $category);
    }

    public function store(CategoryValidator $request)
    {
        return parent::afterStore($request);
    }

    public function destroy(Category $category)
    {
        return parent::afterDestroy($category);
    }
}