
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo $__env->yieldContent('title'); ?> | Gustavo Rodrigo Guillen Villarreal</title>

    <!-- Bootstrap -->
    <link href="<?php echo e(asset('css/bootstrap.min.css')); ?>" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo e(asset('css/font-awesome.min.css')); ?>" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?php echo e(asset('css/custom.min.css')); ?>" rel="stylesheet">
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="#" class="site_title"><i class="fa fa-book"></i> <span>Blog Excercise</span></a>
                </div>

                <div class="clearfix"></div>

                <!-- menu profile quick info -->
                <div class="profile clearfix">
                    <div class="profile_info">
                        <span>Welcome</span>

                        <h2><?php echo e(Auth::check() ? Auth::user()->name : 'Guest'); ?></h2>
                    </div>
                </div>
                <!-- /menu profile quick info -->

                <br />

                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    <div class="menu_section">
                        <h3>General</h3>
                        <ul class="nav side-menu">
                            <li><a href="<?php echo e(route('home')); ?>"><i class="fa fa-home"></i> Home</span></a></li>
                            <?php if(Auth::check()): ?>
                                <?php if(Auth::user()->hasRole('admin')): ?>
                                    <li><a href="<?php echo e(route('posts.index')); ?>"><i class="fa fa-edit"></i> Posts</span></a></li>
                                    <li><a href="<?php echo e(route('categories.index')); ?>"><i class="fa fa-server"></i> Categories</span></a></li>
                                    <li><a href="<?php echo e(route('users.index')); ?>"><i class="fa fa-user"></i> Users </a></li>
                                <?php endif; ?>
                                <li><a href="<?php echo e(route('logout')); ?>" ><i class="fa fa-window-close"></i> Logout </a></li>
                            <?php else: ?>
                                <li><a href="<?php echo e(route('login.index')); ?>"><i class="fa fa-user"></i> Login </a></li>
                            <?php endif; ?>
                        </ul>
                    </div>
                </div>
                <!-- /sidebar menu -->

            </div>
        </div>

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <h3><?php echo $__env->yieldContent('title'); ?> <small><?php echo $__env->yieldContent('subtitle'); ?></small></h3>
                    </div>
                </div>

                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2><?php echo $__env->yieldContent('page_title'); ?></h2>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <?php echo $__env->yieldContent('content'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->

        <!-- /footer content -->
    </div>
</div>

<!-- jQuery -->
<script src="https://code.jquery.com/jquery-3.2.1.min.js"
        integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
        crossorigin="anonymous">

<script src="<?php echo e(asset('js/bootstrap.min.js')); ?>"></script>

<!-- Custom Theme Scripts -->
<script src="<?php echo e(asset('js/custom.min.js')); ?>"></script>
<!-- Google Analytics -->

</body>
</html>