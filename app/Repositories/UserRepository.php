<?php
/**
 * Created by PhpStorm.
 * User: rv
 * Date: 23/07/17
 * Time: 7:39 PM
 */


namespace App\Repositories;

use App\Models\User;
use Hash;

class UserRepository extends Repository
{
    /**
     * Create a new user repository instance.
     *
     * @param  User $user
     * @return void
     */
    public function __construct(User $user)
    {
        $this->entity = $user;
    }

    public function getClass()
    {
        return User::class;
    }

    public function update($entity, $data)
    {
        if(!$data['password']) {
            unset($data['password']);
        } else {
            $data['password'] = Hash::make($data['password']);
        }

        return parent::update($entity, $data);
    }
}