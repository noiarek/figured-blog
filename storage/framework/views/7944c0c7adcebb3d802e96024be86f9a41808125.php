<?php $__env->startSection('title', 'Blog'); ?>

<?php $__env->startSection('content'); ?>
    <?php if(count($posts)): ?>
        <?php $__currentLoopData = $posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h1><?php echo e($post->title); ?></h1>
                            Author: <?php echo e($post->user->name); ?> <br>
                            Category: <?php echo e($post->category->name); ?><br>

                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <?php echo e($post->body); ?>

                        </div>
                    </div>
                </div>
            </div>
            </hr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php else: ?>
        <p>There isn't articles yet!</p>
    <?php endif; ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.base', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>