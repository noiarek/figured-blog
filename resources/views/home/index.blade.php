@extends('layouts.base')

@section('title', 'Blog')

@section('content')
    @if(count($posts))
        @foreach($posts as $post)
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h1>{{ $post->title }}</h1>
                            Author: {{  $post->user->name }} <br>
                            Category: {{  $post->category->name }}<br>

                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            {{ $post->body }}
                        </div>
                    </div>
                </div>
            </div>
            </hr>
        @endforeach
    @else
        <p>There isn't articles yet!</p>
    @endif
@endsection